
data {
  int n;       // number of datapoints
  vector[n] X; // the X and Y values of those datapoints
  vector[n] Y;
}
parameters {
  real m;              // slope
  real b;              // intercept
  real<lower=0> sigma; // standard deviation
}
model {
  vector[n] Y_hat = X*m + b;
  Y ~ normal( Y_hat, sigma );
}

